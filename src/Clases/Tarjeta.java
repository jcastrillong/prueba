/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Pc
 */
public class Tarjeta {
    private long numero;
    private long clave;
    private Cuenta cuentaTarjeta;

    public Tarjeta(long numero, long clave, Cuenta cuentaTarjeta) throws Exception {
        if(numero <= 0) {
            new Exception("Numero de tarjeta incorrecto");
        }

        this.numero = numero;
        this.clave = clave;
        this.cuentaTarjeta = cuentaTarjeta;
    }

    public long getNumero() {
        return numero;
    }

    public long getClave() {
        return clave;
    }

    public Cuenta getCuentaTarjeta() {
        return cuentaTarjeta;
    }
    
    


    
    
    
}
