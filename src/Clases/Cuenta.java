/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Pc
 */
public class Cuenta {
    private long numero;
    private long saldo;
    private Cliente cliente;

    public Cuenta(long numero, long saldo, Cliente cliente) {
        this.numero = numero;
        this.saldo = saldo;
        this.cliente = cliente;
    }

    public long getNumero() {
        return numero;
    }

    public long getSaldo() {
        return saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setSaldo(long saldo) {
        this.saldo = saldo;
    }
    
    
}
