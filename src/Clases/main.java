/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Pc
 */
public class main {

    public static void main(String[] args) {
        try {

            Banco Bancolombia = new Banco("Bancolombia");
            Cliente cliente1 = new Cliente(1006050765, "Jhonatan", "Castrillon");
            Cuenta cuenta1 = new Cuenta(1001, 5000000, cliente1);
            Tarjeta tarjeta1 = new Tarjeta(11111111, 2703, cuenta1);
            Cajero Chipichape = new Cajero(Bancolombia);

            Bancolombia.agregarCliente(cliente1);
            Bancolombia.agregarCuenta(cuenta1);
            Bancolombia.agregarTarjeta(tarjeta1);

            Chipichape.funcionalidad(Bancolombia);
        } catch (Exception exc) {
            System.out.println("ERROR: " + exc.getMessage());
        }
    }
}
