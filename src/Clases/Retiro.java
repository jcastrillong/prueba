/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

/**
 *
 * @author Pc
 */
public class Retiro {
    private long monto;
    private Cuenta retiro_cuenta;

    public Retiro(long monto, Cuenta retiro_cuenta) {
        this.monto = monto;
        this.retiro_cuenta = retiro_cuenta;
    }

    public long getMonto() {
        return monto;
    }

    public Cuenta getRetiro_cuenta() {
        return retiro_cuenta;
    }
    
    
}
