/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.Iterator;
import java.util.LinkedList;
//import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Pc
 */
public class Banco {

    //Atributos
    private String nombre;

    private List<Cliente> listadoClientes = new LinkedList<>();
    private List<Cuenta> listadoCuentas = new LinkedList<>();
    private List<Tarjeta> listadoTarjetas = new LinkedList<>();

    //Método general
    public Banco(String nombre) throws Exception{
        if(nombre == "") {
            throw new Exception("No fue ingresado el nombre del banco");
        }
        
        this.nombre = nombre;
    }

    //Métodos de acceso
    public String getNombre() {
        return nombre;
    }

    //Metodos para agregar objetos a una lista
    public void agregarCliente(Cliente unCliente) {
        this.listadoClientes.add(unCliente);
    }

    public void agregarCuenta(Cuenta unaCuenta) {
        this.listadoCuentas.add(unaCuenta);
    }

    public void agregarTarjeta(Tarjeta unaTarjeta) {
        this.listadoTarjetas.add(unaTarjeta);
    }

    //Otros métodos
    Cuenta buscarTarjeta(long numTarjeta, long clave) throws Exception {
        boolean tarjetaEncontrada = false;
        Cuenta resultado = null;

        for (Tarjeta iterador : listadoTarjetas) {
            if (numTarjeta == iterador.getNumero()) {
                if (clave == iterador.getClave()) {
                    resultado = iterador.getCuentaTarjeta();
                    tarjetaEncontrada = true;
                    break;
                } else {
                    throw new Exception("Contraseña Incorrecta");
                }
            }
            if (tarjetaEncontrada == true) {
                //resultado = iterador.getCuentaTarjeta();
            } else {
                throw new Exception("No se encontró la tarjeta.");
            }
        }

        return resultado;
    }
    
    long devolverSaldo (long numTarjeta , long clave , Retiro montoRetirar) throws Exception {
        long saldoRestante = 0;
        long cuenta = this.buscarTarjeta(numTarjeta, clave).getSaldo();
        
        if(montoRetirar.getMonto() <= cuenta) {
            saldoRestante = (this.buscarTarjeta(numTarjeta, clave).getSaldo()) - (montoRetirar.getMonto());
        } else {
            throw new Exception("No tiene saldo suficiente para efectuar el retiro");
        }
        return saldoRestante;
    }
    
    List<Cuenta> devolverCliente(long numTarjeta , long clave) throws Exception {
        List<Cuenta> clienteDevuelto = new LinkedList<>();
        
        Cuenta cuentaCliente = this.buscarTarjeta(numTarjeta, clave);
        
        clienteDevuelto.add(cuentaCliente);
        
        return clienteDevuelto;
    }
}

