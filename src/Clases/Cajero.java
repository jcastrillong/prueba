/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Pc
 */
public class Cajero {

    private Banco banco;
    private List<Retiro> listadoRetiros = new LinkedList<>();

    public Cajero(Banco banco) {
        this.banco = banco;
        this.listadoRetiros = new LinkedList<>();

    }

    public Banco getBanco() {
        return banco;
    }

    public void agregarRetiros(Retiro unRetiro) {
        this.listadoRetiros.add(unRetiro);
    }

    //Variables
    int intentos = 0;
    long numeroDeCuenta = 0;
    long numeroTarjeta = 0;
    long numtarjetaNueva;
    long clave = 0;
    long claveNueva;
    long numIndNuevo;
    String registroNombres;
    String registroApellidos;
    long montoRetirar;
    int seleccion;

    public void funcionalidad(Banco unBanco) throws Exception {

        System.out.println("------------------------------------------------");
        System.out.println("Bienvenido al cajero " + unBanco.getNombre());

        System.out.println("======================MENÚ======================");
        System.out.println("1. Iniciar Sesión");
        System.out.println("2. Registrarse");
        System.out.println("3. ¿Olvidó su contraseña?");
        System.out.println("================================================");

        Scanner entradaSeleccion = new Scanner(System.in);
        System.out.print("-> ");
        seleccion = entradaSeleccion.nextInt();

        switch (seleccion) {
            case 1: {
                System.out.println("------------------------------------------------");

                Scanner entradaTarjeta = new Scanner(System.in);
                System.out.print("Ingrese el numero de tarjeta: ");
                numeroTarjeta = entradaTarjeta.nextLong();

                Scanner entradaContraseña = new Scanner(System.in);
                System.out.print("Ingrese la contraseña: ");
                clave = entradaContraseña.nextLong();

                unBanco.buscarTarjeta(numeroTarjeta, clave);
                System.out.println("------------------------------------------------");
                System.out.println("SESION INICIADA");
                System.out.println("------------------------------------------------");
                System.out.println("Saldo de la cuenta: " + unBanco.buscarTarjeta(numeroTarjeta, clave).getSaldo());
                System.out.println("------------------------------------------------");

                Scanner entradaSaldoRetirar = new Scanner(System.in);
                System.out.print("Ingrese el monto que desea retirar: ");
                montoRetirar = entradaSaldoRetirar.nextLong();

                Retiro retiro1 = new Retiro(montoRetirar, unBanco.buscarTarjeta(numeroTarjeta, clave));

                unBanco.devolverSaldo(numeroTarjeta, clave, retiro1);

                unBanco.buscarTarjeta(numeroTarjeta, clave).setSaldo(unBanco.devolverSaldo(numeroTarjeta, clave, retiro1));
                System.out.println("------------------------------------------------");
                System.out.println("Transacción exitosa");
                System.out.println("------------------------------------------------");

                this.agregarRetiros(retiro1);

                System.out.println("Información Cliente");
                System.out.println("------------------------------------------------");

                List<Cuenta> informacionCliente = unBanco.devolverCliente(numeroTarjeta, clave);

                for (Cuenta iterador : informacionCliente) {
                    System.out.println("Nombre: " + iterador.getCliente().getNombres());
                    System.out.println("Apellidos: " + iterador.getCliente().getApellidos());
                    System.out.println("Nuevo Saldo: " + iterador.getSaldo());
                }

            }
            case 2 : {
                System.out.println("------------------------------------------------");
                System.out.println("Creación de Cuenta");
                System.out.println("------------------------------------------------");

                Scanner entradanumIndNuevo = new Scanner(System.in);
                System.out.print("Numero de indentificación: ");
                numIndNuevo = entradanumIndNuevo.nextLong();

                Scanner entradaNombres = new Scanner(System.in);
                System.out.print("Nombre: ");
                registroNombres = entradaNombres.next();

                Scanner entradaApellidos = new Scanner(System.in);
                System.out.print("Apellidos: ");
                registroApellidos = entradaApellidos.next();

                System.out.println("------------------------------------------------");
                System.out.println("Obtiene tu tarjeta");
                System.out.println("------------------------------------------------");

                //Scanner entradaTarjetaNueva = new Scanner(System.in);
                //numtarjetaNueva = entradaTarjetaNueva.nextLong();
                numtarjetaNueva = (int) (Math.random() * 100000000);
                System.out.println("Numero de tarjeta: " + numtarjetaNueva);

                Scanner entradaClaveNueva = new Scanner(System.in);
                System.out.print("Contraseña: ");
                claveNueva = entradaClaveNueva.nextLong();

                Cliente nuevoCliente = new Cliente(numIndNuevo, registroNombres, registroApellidos);
                Cuenta nuevaCuenta = new Cuenta(numeroDeCuenta + 1, 0, nuevoCliente);
                Tarjeta nuevaTarjeta = new Tarjeta(numtarjetaNueva, claveNueva, nuevaCuenta);

                unBanco.agregarCliente(nuevoCliente);
                unBanco.agregarCuenta(nuevaCuenta);
                unBanco.agregarTarjeta(nuevaTarjeta);

                List<Cuenta> informacionCliente = unBanco.devolverCliente(numIndNuevo, claveNueva);

                for (Cuenta iterador : informacionCliente) {
                    System.out.println("Nombres: " + iterador.getCliente().getNombres());
                    System.out.println("Apellidos: " + iterador.getCliente().getApellidos());
                    System.out.println("Saldo: " + iterador.getSaldo());
                }
            }
            case 3 : {
                System.out.println("Ésta opción aún no está disponible...!");

            }
            default : {
                intentos++;
                if (intentos <= 3) {
                    System.out.println("La opción " + seleccion + " no existe, intente de nuevo");
                    //System.out.println("..." + intentos);
                    funcionalidad(unBanco);
                } else if (intentos > 3) {
                    throw new Exception("Numero de intentos alcanzados.");
                }
            }
        }

    }
}
